## Getting Started

I use the phpmyadmin as my database for this

1. Clone the project

```bash
git clone https://gitlab.com/dovecancode/taskmanager-be.git
```

2. Install the dependencies:

```bash
yarn
```

3. Run the development server:

```bash
yarn dev
```
