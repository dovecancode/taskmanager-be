import cors from 'cors'
import 'dotenv/config'
import express, { NextFunction, Request, Response, urlencoded } from 'express'
import morgan from 'morgan'
import { createTask, deleteTask, getTask, getTasks, updateTask } from './db'

const app = express()

app.use(morgan('dev'))
app.use(express.json())
app.use(urlencoded({ extended: false }))
app.use(cors())

/**
 * @desc Create new Task
 * @route /api/v1/tasks
 * @access Public
 */
app.get('/api/v1/tasks', async (req, res) => {
  try {
    const tasks = await getTasks()
    res.status(200).json({ status: 'success', data: tasks })
  } catch (error) {
    console.log((error as Error).message)
    res.status(400).json({ status: 'failed', error: (error as Error).message })
  }
})

/**
 * @desc GET Task
 * @route /api/v1/tasks/:id
 * @access Public
 */
app.get('/api/v1/tasks/:id', async (req, res) => {
  try {
    const task = await getTask(req.params.id)
    res.status(200).json({ status: 'success', data: task })
  } catch (error) {
    console.log((error as Error).message)
    res.status(400).json({ status: 'failed', error: (error as Error).message })
  }
})

/**
 * @desc CREATE Task
 * @route /api/v1/tasks/
 * @access Public
 */
app.post('/api/v1/tasks/', async (req, res) => {
  try {
    const results = await createTask(req.body.title, req.body.status)
    console.log(results)
    res.status(201).json({ status: 'success', data: results })
  } catch (error) {
    console.log((error as Error).message)
    res.status(400).json({ status: 'failed', error: (error as Error).message })
  }
})

/**
 * @desc UPDATE Task
 * @route /api/v1/tasks/:id
 * @access Public
 */
app.put('/api/v1/tasks/:id', async (req, res) => {
  try {
    const results = await updateTask(
      req.body.title,
      req.body.status,
      req.params.id
    )
    console.log(results)

    res.status(200).json({ status: 'success', data: results })
  } catch (error) {
    console.log((error as Error).message)
    res.status(400).json({ status: 'failed', error: (error as Error).message })
  }
})

/**
 * @desc DELETE Task
 * @route /api/v1/tasks/:id
 * @access Public
 */

app.delete('/api/v1/tasks/:id', async (req, res) => {
  try {
    const results = await deleteTask(req.params.id)
    res.status(200).json({ status: 'success', data: results.affectedRows })
  } catch (error) {
    console.log((error as Error).message)
    res.status(400).json({ status: 'failed', error: (error as Error).message })
  }
})

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

const PORT = process.env.PORT || 3001

app.listen(PORT, () => {
  console.log('server is running in port ' + PORT)
})
