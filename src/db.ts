import mysql, {
  PoolOptions,
  ResultSetHeader,
  RowDataPacket,
} from 'mysql2/promise'

const access: PoolOptions = {
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  database: process.env.MYSQL_DATABASE,
  password: process.env.MYSQL_PASSWORD,
}

const pool = mysql.createPool(access)

type ITaskRow = RowDataPacket & {
  id: number
  title: string
  status: string
}

export async function getTasks() {
  const [rows] = await pool.execute<ITaskRow[]>('SELECT * FROM tasks')
  return rows
}

export async function getTask(id: string) {
  const [rows] = await pool.execute<ITaskRow[]>(
    'SELECT * FROM `tasks` WHERE `id` = ?',
    [id]
  )
  return rows
}

export async function createTask(title: string, status: string) {
  const sql = 'INSERT INTO `tasks`(`title`, `status`) VALUES (?, ?)'
  const values = [title, status]
  const [results] = await pool.execute<ResultSetHeader>(sql, values)
  const id = results.insertId
  return getTask(String(id))
}

export async function updateTask(title: string, status: string, id: string) {
  const sql =
    'UPDATE `tasks` SET `title` = ?, `status` = ? WHERE `id` = ? LIMIT 1'
  const values = [title, status, id]
  const [results] = await pool.execute<ResultSetHeader>(sql, values)
  // const taskId = results.insertId
  // return getTask(String(taskId))
  return getTask(id)
}

export async function deleteTask(id: string) {
  const sql = 'DELETE FROM `tasks` WHERE `id` = ? LIMIT 1'
  const values = [id]
  const [results] = await pool.execute<ResultSetHeader>(sql, values)
  return results
}
